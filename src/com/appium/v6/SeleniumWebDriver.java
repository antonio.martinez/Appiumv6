package com.appium.v6;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;


public class SeleniumWebDriver {
	
	WebDriver driver;
	
	@Before
	public void setUp() throws MalformedURLException 
	{
		try {
			String _driverPath = "C:\\Temp\\JavaCABuild\\Drivers\\chromedriver.exe";
			System.setProperty("webdriver.chrome.driver", _driverPath);
			
			
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			driver = new ChromeDriver(options);						
			
		} catch(Exception ex) {
			System.out.print(ex.getMessage());
		}
	}
	
	@Test
	public void Test() {
		driver.get("http://www.google.com");
	}
		
}
