package com.appium.v6;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServerHasNotBeenStartedLocallyException;
import io.appium.java_client.service.local.AppiumServiceBuilder;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.URL;


public class Appiumv6 {
	
	public AppiumDriver<MobileElement> driver;
	AppiumDriverLocalService service;
	MobileElement elmnt;
	
	@Before
	public void setUp() throws Exception 
	{				
		/*
		   AppiumDriverLocalService service = AppiumDriverLocalService
		 
				.buildService(new AppiumServiceBuilder()
				.usingDriverExecutable(new File("C:/Program Files/nodejs/node.exe"))
				.withAppiumJS(new File("C:/Users/36577/AppData/Local/Programs/appium-desktop/resources/app/node_modules/appium/lib/appium.js"))
				.withLogFile(new File("C:/Users/36577/Desktop/appiumLogs.txt"))
				.withIPAddress("0.0.0.0")
				.usingPort(4723));

			service.start();
			if(service == null || !service.isRunning()) {
				throw new AppiumServerHasNotBeenStartedLocallyException("An appium server node is not started.");
			}
		*/
		
		String _apkPath = "D:\\git\\STMJava\\Test\\Test\\apks\\ApiDemos-debug.apk";		
		
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("deviceName", "Moto G4");
		caps.setCapability("app", _apkPath);
		//caps.setCapability("appPackage", "io.appium.android.apis");
		//caps.setCapability("appActivity", ".ApiDemos");		
		/* 
		 *  Using AppiumDriverLocalService
		 *  	driver = new AndroidDriver<>(service.getUrl(), caps);
		 */
		driver = new AndroidDriver<>(new URL("http://0.0.0.0:4723/wd/hub"), caps);
	}
	
	@Test
	public void Test() throws InterruptedException {		
		try {
			driver.launchApp();
			elmnt = (MobileElement) driver.findElementById("Views");		
			elmnt.click();		
			Thread.sleep(5000);
		} 
		catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	@Ignore
	public void Views() {
		elmnt = (MobileElement) driver.findElementById("Views");
		elmnt.click();
	}
	
	@After
	public void tearDown() throws Exception {
		if (driver != null) {
			driver.quit();
		}
		
		if (service != null) {
			service.stop();
		}
	}
		
}
